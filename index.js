// importe le module Express
const express = require('express');
const app = express();
// on utilisera le port 3000 pour accéder au serveur
const port = 3000;

// définition d'une route GET sur l'URL racine ('/')
app.get('/', (req, res) => {
  // envoie une réponse 'Hello World!' au client
  res.send('Hello World!');
})

// définition d'une route GET sur l'URL /about
app.get('/about', (req, res) => {
    // envoie une réponse 'About me' au client
    res.send('About me');
  })

// démarrage du serveur sur le port défini
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
})

app.get('/data', (req, res) => {
    // envoie un objet JSON en réponse
    res.send({ name: 'Pikachu', power: 20, life: 50 });
  });
  
